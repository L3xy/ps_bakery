<html lang ="en">
<head>
	<title> API Consume</title>
	<style>

	input, select {font-size: 1.2em;width:97%}
	td {font-size: 1.2em;}
	legend { font-size:1.3em;}
	</style>
</head>
<body>
<center>


			    <fieldset style="width:40%; background-color:green;border-radius:30px">
	<legend>OTP Generated</legend>
	<form name="otp" method="POST">
	
	<table>
		<tr>
			<td> Transfer Code</td> <td><input type="text" name="transfer_code" value="<?php echo $transferCode; ?>" readonly></td>
		</tr>
		<tr>
			<td>OTP <font color="red">(required)</font> : </td> <td> <input type="text" name="otp" required></td>
		</tr>
		<tr>
			<td colspan=2 align=center><input type="submit" value="Authorize Transfer!" name="cmdotp"></td>
		</tr>
	</table>

	</form>
</fieldset>


<fieldset style="width:40%">
	<legend>Create A New Recipient</legend>
	<form name="createRe" onsubmit="return validateField()" method="POST">			
			<table>
			<tr>
				<td>Full Name : </td> <td><input type="text" value="<?php echo $name; ?>" name="txtname" required></td>
			</tr>
			<tr>
				<td>Description : </td><td><input type="text" value="<?php echo $desc; ?>" name="txtdesc" required></td>
			</tr>
			<tr>
				<td>Account Number : </td><td><input type="text" value="<?php echo $account; ?>" name="txtaccount" required></td>
			</tr>
			<tr>
				<td>Bank Name : </td><td><select name="bankname">
				    <option value="044">Access Bank</option>
					<option value="023">Citibank Nigeria</option>
					<option value="063">Diamond Bank</option>
					<option value="050">Ecobank Nigeria</option>
					<option value="084">Enterprise Bank</option>
					<option value="070">Fidelity Bank</option>
					<option value="011">Firstbank</option>
					<option value="214">First City Monument Bank</option>
					<option value="058">Guaranty Trust Bank</option>
					<option value="030">Heritge Bank</option>
					<option value="082">Keystone Bank</option>
					<option value="014">Mainstreet Bank</option>
					<option value="076">Skye Bank</option>
					<option value="221">Stanbic IBTC Bank</option>
					<option value="068">Standard Chartered Bank</option>
					<option value="232">Sterling Bank</option>
					<option value="032">Union Bank Of Nigeria</option>
					<option value="033">United Bank for Africa</option>
					<option value="215">Unity Bank</option>
					<option value="035">Wema Bank</option>
					<option value="057">Zenith Bank</option>
				</select></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="cmdrep" value="Create Recipient"> </td>
			</tr>
			</table>	
		
	</form>
</fieldset>

<fieldset style="width:40%">
	<legend>Transfer To A Recipient</legend>
	<form name="trans" method="POST">
	
	<table>
		<tr>
			<td> Select Transfer Recipient</td> <td><select name="recTran">
				</select>
			</td>
		</tr>
		<tr>
			<td>Amount : </td> <td> <input type="text" name="amtTran"></td>
		</tr>
		<tr>
			<td>Reason : </td> <td> <input type="text" name="reason"></td>
		</tr>
		<tr>
			<td colspan=2 align=center><input type="submit" value="Transfer Now!" name="cmdtran"></td>
		</tr>
	</table>

	</form>
</fieldset>

</center>
</body>
</html>