<html>
	<head>
		<title>Verify Account Number</title>
		<style>

	input, select {font-size: 1.2em;width:97%}
	td {font-size: 1.2em;}
	legend { font-size:1.3em;}
	</style>
	</head>
	<body>
	<body>
	<?php
		// header("Content-Type: application/json; charset=UTF-8");
		$account = $_POST['txtaccount'];
		$bankname = $_POST['bankname'];

		if(isset($_POST['cmdBank']))
		{
				$curl = curl_init();
				$url = "https://api.paystack.co/bank/resolve?account_number=$account&bank_code=$bankname";
				curl_setopt_array($curl, array(
				  CURLOPT_URL => $url,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer sk_test_36e175c5c710aacac84e2a3974988707c0834e7d",
				    "Cache-Control: no-cache",
				    "Content-Type: application/json"
				  )
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				$ans = json_decode($response);
				$accName = $ans->data->account_name;

				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
					echo "<p align=center> $account belongs to <b> $accName </b> <p/>";
				}
				}
	?>

		<center>
		<fieldset style="width:40%">
	<legend>Verify Account Number</legend>
	<form name="createRe" onsubmit="return validateField()" method="POST">			
			<table cellspacing=5 cellpadding=5>
			<tr>
				<td>Account Number : </td><td><input type="text" placeholder="e.g 0230034000" name="txtaccount" required></td>
			</tr>
			<tr>
				<td>Bank Name : </td><td><select name="bankname">
				    <option value="044">Access Bank</option>
					<option value="023">Citibank Nigeria</option>
					<option value="063">Diamond Bank</option>
					<option value="050">Ecobank Nigeria</option>
					<option value="084">Enterprise Bank</option>
					<option value="070">Fidelity Bank</option>
					<option value="011">Firstbank</option>
					<option value="214">First City Monument Bank</option>
					<option value="058">Guaranty Trust Bank</option>
					<option value="030">Heritge Bank</option>
					<option value="082">Keystone Bank</option>
					<option value="014">Mainstreet Bank</option>
					<option value="076">Skye Bank</option>
					<option value="221">Stanbic IBTC Bank</option>
					<option value="068">Standard Chartered Bank</option>
					<option value="232">Sterling Bank</option>
					<option value="032">Union Bank Of Nigeria</option>
					<option value="033">United Bank for Africa</option>
					<option value="215">Unity Bank</option>
					<option value="035">Wema Bank</option>
					<option value="057">Zenith Bank</option>
				</select></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="cmdBank" value="Get Account Name"> </td>
			</tr>
			</table>	
		
	</form>
</fieldset>
</center>
	</body>
</html>