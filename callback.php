<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
$reqBody = file_get_contents("php://input");
$data = json_decode($reqBody);

$myfile = file_put_contents('Callback.txt', $data.PHP_EOL , FILE_APPEND | LOCK_EX);

?>
<html lang="en">
<head><title>Callback</title></head>
<body>
    <center>Nothing here</center>
</body>
</html>